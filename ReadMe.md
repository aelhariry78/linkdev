### Link development evaluation task 

#### to run DB migration use the following command
  - `cd LinkDev.RecruitmentApi` folder 
  - `dotnet ef migrations add -c ApplicationDbContext -o Infrastructure/Presistance/Migrations`
  - `dotnet ef database update`

#### fronted 
  - builded and developed by vuejs 
    - **to run it**
	  - `cd ef`
	  - `npm run install --force`
	  - `npm run serve`

##### Documentation 
  - I provided this project with db seeds to initialize the main lookups 
  - you can login using the following credentials 
	- `username: admin & pass: Admin@123`
  - you the main CRUD oprations for entities like 
	- skills
	- categories
	- job vcancies 

  - **login page**
	<img src="./docs/login.png" />
  - **Skills**
	<img src="./docs/skills.png" />
	<img src="./docs/skills-edit.png" />
  - **categories**
	<img src="./docs/skills.png" />
	<img src="./docs/skills-edit.png" />
  - **vacancies**
	<img src="./docs/vacancies.png" />
	<img src="./docs/vacancies-edit.png" />
	<img src="./docs/vacancies-edit2.png" />
  - **Applay**
	<img src="./docs/apply.png" />
	<img src="./docs/apply2.png" />
- **approach**
  - I tried to follow the structured recommended by Microsoft as figured out below
  - I exclude some layer like service and unit of work due to the simplicity of the project

    <img src="./docs/ddd-service-layer-dependencies.png" />
    <img src="./docs/domain-driven-design-microservice.png" />

- **References**
  - [global update for data](https://www.entityframeworktutorial.net/faq/set-created-and-modified-date-in-efcore.aspx)
