﻿using Microsoft.EntityFrameworkCore;
using System.Reflection;
using LinkDev.CoreApiBuilder.Application.Extensions;
using LinkDev.CoreApiBuilder.Infrastructure;
using LinkDev.CoreApiBuilder.Domain.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.OpenApi.Models;
using LinkDev.CoreApiBuilder.Application.Configurations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using System.Text.Json.Serialization;

namespace LinkDev.CoreApiBuilder
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            // Configure the HTTP request pipeline.
            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
                app.UseDeveloperExceptionPage();
            }


            app.UseAuthentication();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(x => x.MapControllers());
            app.UseHttpsRedirection();


        }

        public virtual void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
                    .AddJsonOptions(options =>
                    {
                        options.JsonSerializerOptions.Converters.Add(new DateTimeConverter());
                        options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                    });

            services.AddCors(x =>
            {
                x.AddDefaultPolicy(p =>
                {
                    p.AllowAnyOrigin();
                    p.AllowAnyMethod();
                    p.AllowAnyHeader();
                    p.AllowCredentials();
                });
            });
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Link Development",
                    Version = "v1"
                });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Please insert JWT with Bearer into field",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement {
                   {
                     new OpenApiSecurityScheme
                     {
                       Reference = new OpenApiReference
                       {
                         Type = ReferenceType.SecurityScheme,
                         Id = "Bearer"
                       }
                      },
                      new string[] { }
                    }
                  });
            });

            var runningAssembly = Assembly.GetEntryAssembly().GetName().Name;
            runningAssembly = "LinkDev.RecruitmentApi";
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options
                .UseSqlServer(_configuration.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly(runningAssembly))
                .LogTo(Console.Write);
                
            });
            services.AddSingleton(new DbContextFactory(options => options.UseSqlServer(_configuration.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly(runningAssembly))));
            services.AddIdentity<ApplicationUser, IdentityRole>().AddEntityFrameworkStores<ApplicationDbContext>();


            services.ConfigureAuthontication(_configuration).
                    RegisterService().
                    RegisterDbInitializer();
        }
    }
}
