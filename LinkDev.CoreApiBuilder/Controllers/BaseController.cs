﻿using LinkDev.CoreApiBuilder.Application.Dtos;
using LinkDev.CoreApiBuilder.Domain.Models;
using LinkDev.CoreApiBuilder.Domain.Repositories;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Drawing;

namespace LinkDev.CoreApiBuilder.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class BaseController<TEntity, TDto> : ControllerBase where TEntity : Entity
    {
        private readonly IRepository<TEntity> _repository;

        public BaseController(IRepository<TEntity> repository)
        {
            _repository = repository;
        }

        [HttpGet]
        [Route("{id}")]
        public virtual async Task<ActionResult<TDto>> Get(int id)
        {
            var record = await _repository.GetByIdAsync(id);
            if (record == null)
                return NotFound();
            return Ok(record.Adapt<TDto>());
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<TDto>>> GetAll()
        {
            var records = await _repository.GetAllAsync();
            if (records == null)
                return NotFound();
            return Ok(records.Adapt<IEnumerable<TDto>>());
        }

        [HttpGet]
        [Route("Page/{pageId}")]
        public virtual async Task<ActionResult<PagerDto<TDto>>> GetPages(int pageId)
        {
            var records = await _repository.GetPage(pageId);
            return Ok(new PagerDto<TDto>() {  Total = records.Total , Data = records.Data.Select(x => x.Adapt<TDto>()).ToList() });
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<TDto>> Create(TDto entity)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var record = await _repository.CreateAsync(entity.Adapt<TEntity>());
            return CreatedAtAction(nameof(Get), new { id = record.Id }, record);
        }

        [HttpPut]
        [Route("{Id}")]
        public async Task<ActionResult<TDto>> Put(int id,TDto entity)
        {
            var record = await _repository.GetByIdAsync(id);
            entity.Adapt(record);
            await _repository.UpdateAsync(record);
            return Ok(entity);
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<ActionResult<TDto>> Delete(int id)
        {
            await _repository.DeleteAsync(id);
            return Ok();
        }
    }
}
