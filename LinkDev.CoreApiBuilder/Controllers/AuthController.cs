﻿using LinkDev.CoreApiBuilder.Application.Dtos;
using LinkDev.CoreApiBuilder.Application.Security;
using LinkDev.CoreApiBuilder.Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace LinkDev.CoreApiBuilder.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IJWTGenerator _jWTGenerator;
        public AuthController(SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager, IJWTGenerator jWTGenerator)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _jWTGenerator = jWTGenerator;
        }

        [HttpPost]
        public async Task<IActionResult> Post(AuthDto authDto)
        {
            var user = await _userManager.FindByNameAsync(authDto.UserName);
            if (user == null) 
                return Unauthorized();

            var result = await _signInManager.CheckPasswordSignInAsync(user,authDto.Password, false);
            if (result.Succeeded)
                return Ok(new { token = _jWTGenerator.GenerateToken(user.Id, authDto.UserName, user.Email) });
                
          return Unauthorized();
        }
    }
}
