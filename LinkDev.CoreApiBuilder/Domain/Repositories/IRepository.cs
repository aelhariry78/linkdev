﻿using LinkDev.CoreApiBuilder.Application.Dtos;
using LinkDev.CoreApiBuilder.Domain.Models;
using System.Linq.Expressions;

namespace LinkDev.CoreApiBuilder.Domain.Repositories
{
    public interface IRepository<TEntity> where TEntity : Entity
    {
        Task<TEntity> GetByIdAsync(int id);
        Task<IEnumerable<TEntity>> GetAllAsync();
        Task<TEntity> CreateAsync(TEntity entity);
        Task<TEntity> UpdateAsync(TEntity entity);
        Task DeleteAsync(int id);
        Task<bool> IsExist(Expression<Func<TEntity, bool>> predict);
        Task<PagerDto<TEntity>> GetPage(int page, int length = 3);
        Task<IEnumerable<TEntity>> GetAllAsync(params int[] ids);
    }

}
