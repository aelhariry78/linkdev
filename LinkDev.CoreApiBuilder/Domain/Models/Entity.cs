﻿using System.ComponentModel.DataAnnotations;

namespace LinkDev.CoreApiBuilder.Domain.Models
{
    public abstract class Entity
    {
        [Key]
        public int Id { get; set; }

        #region Audit
        public string? CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public string? UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        #endregion
    }
}
