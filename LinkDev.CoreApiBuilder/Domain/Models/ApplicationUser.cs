﻿using Microsoft.AspNetCore.Identity;

namespace LinkDev.CoreApiBuilder.Domain.Models
{
    public class ApplicationUser:IdentityUser
    {
    }
}
