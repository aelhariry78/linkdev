﻿using LinkDev.CoreApiBuilder.Application.Configurations;
using LinkDev.CoreApiBuilder.Application.Security;
using LinkDev.CoreApiBuilder.Domain.Repositories;
using LinkDev.CoreApiBuilder.Infrastructure.Repositories;
using LinkDev.CoreApiBuilder.Infrastructure.Seeds;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Reflection;
using System.Text;

namespace LinkDev.CoreApiBuilder.Application.Extensions
{
    public static class ServiceExtensions
    {
        public static IServiceCollection RegisterService(this IServiceCollection services)
        {
            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient(typeof(IJWTGenerator), typeof(JWTGenerator));
            services.AddScoped(typeof(ILoginedUser), typeof(LoginedUser));
            return services;
        }

        public static IServiceCollection RegisterDbInitializer(this IServiceCollection services)
        {
            AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes()).
                Where(x => x.IsClass && !x.IsAbstract && x.IsAssignableTo(typeof(IDbInitializer))).ToList().ForEach(dbInit =>
                {
                    var interfaces = dbInit.GetInterfaces();
                    foreach (var iface in interfaces)
                    {
                        services.AddScoped(iface, dbInit);
                    }
                });
            return services;
        }

        public static IServiceCollection ConfigureAuthontication(this IServiceCollection services, IConfiguration configuration)
        {
            var jwtOptions = configuration.GetSection("jwt").Get<JwtOptions>();
            services.AddSingleton(jwtOptions);
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(o =>
            {
                o.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidIssuer = jwtOptions.Issuer,
                    ValidAudience = jwtOptions.Audience,
                    IssuerSigningKey = new SymmetricSecurityKey
                    (Encoding.UTF8.GetBytes(jwtOptions.Key)),
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    RequireExpirationTime = true,
                };
            });
            services.AddAuthorization();
            return services;
        }
    }
}
