﻿namespace LinkDev.CoreApiBuilder.Application.Security
{
    public interface ILoginedUser
    {
        public bool IsAuthorized { get;  }
        public string Id { get; }
        public string UserName { get; }
    }
}
