﻿using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using System.Security.Claims;

namespace LinkDev.CoreApiBuilder.Application.Security
{
    public class LoginedUser : ILoginedUser
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IEnumerable<Claim> _claims;
        private readonly ILogger<LoginedUser> _logger;
        public LoginedUser(IHttpContextAccessor contextAccessor, ILogger<LoginedUser> logger = null)
        {
            _contextAccessor = contextAccessor;
            _logger = logger;
        }

        public bool IsAuthorized => _contextAccessor.HttpContext.User.Identity.IsAuthenticated;
        public string Id => _contextAccessor.HttpContext.User.Claims.First(c => c.Type == JwtRegisteredClaimNames.Identifier).Value;

        public string UserName => _contextAccessor.HttpContext.User.Claims.First(c => c.Type == JwtRegisteredClaimNames.UserName).Value;
    }
}
