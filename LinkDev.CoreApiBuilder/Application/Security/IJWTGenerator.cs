﻿namespace LinkDev.CoreApiBuilder.Application.Security
{
    public interface IJWTGenerator
    {
        string GenerateToken(string id ,string userName, string email);
    }
}
