﻿using LinkDev.CoreApiBuilder.Application.Configurations;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace LinkDev.CoreApiBuilder.Application.Security
{
    public class JWTGenerator: IJWTGenerator
    {
        private readonly JwtOptions _jwtOptions;
        public JWTGenerator(JwtOptions jwtOptions = null)
        {
            _jwtOptions = jwtOptions;
        }
        public string GenerateToken(string id,string userName, string email)
        {
            var key = Encoding.ASCII.GetBytes(_jwtOptions.Key);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                new Claim(JwtRegisteredClaimNames.Identifier, id),
                new Claim(JwtRegisteredClaimNames.UserName, userName),
                new Claim(JwtRegisteredClaimNames.Email, email),
             }),
                Expires = DateTime.UtcNow.AddMinutes(30),
                Issuer = _jwtOptions.Issuer,
                Audience = _jwtOptions.Audience,
                SigningCredentials = new SigningCredentials
                (new SymmetricSecurityKey(key),
                SecurityAlgorithms.HmacSha512Signature)
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
