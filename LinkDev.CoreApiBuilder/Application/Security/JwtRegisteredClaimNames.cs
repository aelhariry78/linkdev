﻿using System.Security.Claims;

namespace LinkDev.CoreApiBuilder.Application.Security
{
    public  class JwtRegisteredClaimNames: ClaimsIdentity
    {
        public  JwtRegisteredClaimNames()
        {
            
        }

        public const string Identifier = "__id__";
        public const string UserName = "__username__";
        public const string Email = "__Email__";

       
    }
}
