﻿namespace LinkDev.CoreApiBuilder.Application.Dtos
{
    public class MessageResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
