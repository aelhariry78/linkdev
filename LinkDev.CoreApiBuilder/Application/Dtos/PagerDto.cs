﻿namespace LinkDev.CoreApiBuilder.Application.Dtos
{
    public class PagerDto<TModel>
    {
        public List<TModel> Data { get; set; }
        public int Total { get; set; }
    }
}
