﻿namespace LinkDev.CoreApiBuilder.Application.Dtos
{
    public class LookupDto
    {
        public int Id { get; set; }
        public string LocalName { get; set; }
        public string ForeignName { get; set; }
    }
}
