﻿using Microsoft.EntityFrameworkCore;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Infrastructure.Pluralization;
using System.Reflection;

namespace LinkDev.CoreApiBuilder.Infrastructure.Extensions
{
    public static class ModelBuilderExtension
    {
        public static ModelBuilder RegisterAllEntities<TEntity> (this ModelBuilder modelBuilder, params Assembly[] assemblies)
        {
            assemblies.SelectMany(assembly => assembly.GetTypes()).Where(t => !t.IsAbstract && t.IsAssignableTo(typeof(TEntity))).ToList().ForEach(type =>
            {
                modelBuilder.Entity(type, builder =>
                {
                    var pluralizationService = new EnglishPluralizationService();
                    builder.ToTable(pluralizationService.Pluralize(type.Name));
                });
            });
            return modelBuilder;
        }
    }
}
