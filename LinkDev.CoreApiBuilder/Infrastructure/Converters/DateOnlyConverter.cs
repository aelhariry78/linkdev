﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System.Linq.Expressions;

namespace LinkDev.CoreApiBuilder.Infrastructure.Converters
{
    public class DateOnlyConverter : ValueConverter<DateOnly, DateTime>
    {
        public DateOnlyConverter()
         : base(dateOnly =>
                 dateOnly.ToDateTime(TimeOnly.MinValue),
             dateTime => DateOnly.FromDateTime(dateTime))
        {
        }
    }
}
