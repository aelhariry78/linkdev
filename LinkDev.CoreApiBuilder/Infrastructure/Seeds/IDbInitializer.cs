﻿namespace LinkDev.CoreApiBuilder.Infrastructure.Seeds
{
    public interface IDbInitializer
    {
        Task Seed();
    }
}
