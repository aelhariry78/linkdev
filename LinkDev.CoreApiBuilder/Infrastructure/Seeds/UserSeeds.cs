﻿using LinkDev.CoreApiBuilder.Domain.Models;
using Microsoft.AspNetCore.Identity;

namespace LinkDev.CoreApiBuilder.Infrastructure.Seeds
{
    public class UserSeeds:IDbInitializer
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<UserSeeds> _logger;
        public UserSeeds(UserManager<ApplicationUser> userManager, ILogger<UserSeeds> logger = null)
        {
            _userManager = userManager;
            _logger = logger;
        }

        public async Task Seed() {
            var defaultPass = "Admin@123";
            var user = new ApplicationUser() { UserName = "admin", Email = "aelhariry78@yahoo.com" };

            if (_userManager.Users.FirstOrDefault(x => x.UserName == "admin") == null)
            {
                var result = _userManager.CreateAsync(user, defaultPass).GetAwaiter().GetResult();
                if (!result.Succeeded)
                    _logger.LogError($"there is an error while populating admin user {result.Errors.FirstOrDefault()?.Description}");
            }

    
        }
    }
}
