﻿using Azure;
using LinkDev.CoreApiBuilder.Application.Dtos;
using LinkDev.CoreApiBuilder.Domain.Models;
using LinkDev.CoreApiBuilder.Domain.Repositories;
using Mapster;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.Linq.Expressions;

namespace LinkDev.CoreApiBuilder.Infrastructure.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        private readonly ApplicationDbContext _context;
        
        public Repository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<TEntity> CreateAsync(TEntity entity)
        {
            await AsQueryable().AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task DeleteAsync(int id)
        {
            await AsQueryable().Where(x => x.Id == id).ExecuteDeleteAsync();
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await AsQueryable().ToListAsync();
        }
        /// <summary>
        /// get all with given id 
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<IEnumerable<TEntity>> GetAllAsync(params int[] ids)
        { 
            return await AsQueryable().Where(x => ids.Contains(x.Id)).ToListAsync();
        }
        public virtual async Task<TEntity> GetByIdAsync(int id)
        {
            return await AsQueryable().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            await _context.SaveChangesAsync();
            return entity;
        }
        public async Task<bool> IsExist(Expression<Func<TEntity, bool>> predict)
        {
            var isExist = (await AsQueryable().Where(predict).Select(x => x.Id).FirstOrDefaultAsync()) > 0;
            return isExist;
        }
        protected virtual DbSet<TEntity> AsQueryable()
        {
            var query =  _context.Set<TEntity>();

            foreach (var entity in _context.Model.FindEntityType(typeof(TEntity)).GetNavigations())
            {
                if (typeof(TEntity).GetProperties().FirstOrDefault(x => x.Name == entity.Name) != null)
                    query.Include(entity.Name).Load();
            }
            return query;
        }

        
        public virtual async Task<PagerDto<TEntity>> GetPage(int page, int length = 3)
        {
            var skip = page * length;
            return new()
            {
                Data = await AsQueryable().Skip(skip).Take(length).ToListAsync(),
                Total = AsQueryable().Count()
            };
        }
    }
}
