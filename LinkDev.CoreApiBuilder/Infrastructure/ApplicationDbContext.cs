﻿using LinkDev.CoreApiBuilder.Application.Security;
using LinkDev.CoreApiBuilder.Domain.Models;
using LinkDev.CoreApiBuilder.Infrastructure.Converters;
using LinkDev.CoreApiBuilder.Infrastructure.Extensions;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Reflection;
using System.Reflection.Emit;

namespace LinkDev.CoreApiBuilder.Infrastructure
{
    public class ApplicationDbContext:IdentityDbContext<ApplicationUser>
    {
        private readonly ILoginedUser _loginedUser;
        public ApplicationDbContext(DbContextOptions options, ILoginedUser loginedUser) : base(options)
        {
            _loginedUser = loginedUser;
        }

        protected override void ConfigureConventions(ModelConfigurationBuilder configurationBuilder)
        {
            configurationBuilder.Properties<DateOnly>()
                            .HaveConversion<DateOnlyConverter>()
                            .HaveColumnType("date");
          
            base.ConfigureConventions(configurationBuilder);
        }

        
        public override int SaveChanges()
        {
            ChangeTracker.Entries().ToList().ForEach(entity =>
            {
                if (entity is Entity && entity.State == EntityState.Added)
                {
                    entity.Property(nameof(Entity.CreatedBy)).CurrentValue = _loginedUser.Id;
                    entity.Property(nameof(Entity.CreatedAt)).CurrentValue = DateTime.Now;
                }

                if (entity is Entity && entity.State == EntityState.Modified)
                {
                    entity.Property(nameof(Entity.UpdatedBy)).CurrentValue = _loginedUser.Id;
                    entity.Property(nameof(Entity.UpdatedAt)).CurrentValue = DateTime.Now;
                }
            });

            return base.SaveChanges();
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {

            builder.ApplyConfigurationsFromAssembly(AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(x => x.GetName().Name == "LinkDev.RecruitmentApi"));
            builder.RegisterAllEntities<Entity>(AppDomain.CurrentDomain.GetAssemblies());
            base.OnModelCreating(builder);
        }
    }
}
