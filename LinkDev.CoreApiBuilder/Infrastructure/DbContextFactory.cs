﻿using LinkDev.CoreApiBuilder.Application.Security;
using Microsoft.EntityFrameworkCore;

namespace LinkDev.CoreApiBuilder.Infrastructure
{
    public class DbContextFactory
    {
        private readonly Action<DbContextOptionsBuilder> DbBuilder;
        // private readonly ILoginedUser _loginedUser;

        public DbContextFactory(Action<DbContextOptionsBuilder> dbBuilder)
        {
            DbBuilder = dbBuilder;
        }

        public ApplicationDbContext CreateInstance()
        {
            DbContextOptionsBuilder dbContextOptionsBuilder = new DbContextOptionsBuilder();
            DbBuilder(dbContextOptionsBuilder);
            // return new ApplicationDbContext(dbContextOptionsBuilder.Options);
            return null;
        }
    }
}
