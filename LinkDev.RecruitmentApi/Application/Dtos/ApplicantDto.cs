﻿using System.ComponentModel.DataAnnotations;

namespace LinkDev.RecruitmentApi.Application.Dtos
{
    public class ApplicantDto
    {
        public int JobId { get; set; }
        [MaxLength(150)]
        public string FullName { get; set; }

        [EmailAddress]
        [MaxLength(150)]
        public string Email { get; set; }
        [MaxLength(150)]
        public string PhoneNumber { get; set; }
        public DateTime AppliedAt { get; set; }
    }
}
