﻿namespace LinkDev.RecruitmentApi.Application.Dtos
{
    public class JobVacancySkillDto
    {

        public int JobVacancyId { get; set; }

        public int SkillId { get; set; }
        public string? LocalName { get; set; }
        public string? ForeignName { get; set; }
    }
}
