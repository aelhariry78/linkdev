﻿using LinkDev.RecruitmentApi.Domain.Models;
using System.ComponentModel.DataAnnotations;

namespace LinkDev.RecruitmentApi.Application.Dtos
{
    public class JobVacancyDto
    {
        public int Id { get; set; }
        [MaxLength(150)]
        public string LocalName { get; set; }
        [MaxLength(150)]
        public string ForeignName { get; set; }
        [MaxLength(500)]
        public string LocalDescription { get; set; }
        [MaxLength(500)]
        public string ForeignDesciption { get; set; }
        [MaxLength(500)]
        public string Responsibilities { get; set; }
        public DateOnly ValidFrom { get; set; }
        public DateOnly ValidTo { get; set; }
        public int MaximumApplication { get; set; }
        public int JobCategoryId { get; set; }
        public  List<JobVacancySkillDto> JobVacancySkills { get; set; } = new();
        public List<ApplicantDto> Applicants { get; set; } = new();
    }
}
