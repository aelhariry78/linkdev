﻿using LinkDev.CoreApiBuilder.Application.Dtos;
using LinkDev.CoreApiBuilder.Application.Security;
using LinkDev.CoreApiBuilder.Controllers;
using LinkDev.CoreApiBuilder.Domain.Repositories;
using LinkDev.CoreApiBuilder.Infrastructure.Repositories;
using LinkDev.RecruitmentApi.Application.Dtos;
using LinkDev.RecruitmentApi.Domain.Models;
using LinkDev.RecruitmentApi.Domain.Repositories;
using Mapster;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Reflection.Metadata.Ecma335;

namespace LinkDev.RecruitmentApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobVacanciesController : BaseController<JobVacancy, JobVacancyDto>
    {
        private readonly IJobVacancyRepository _repository;
        private readonly IRepository<Skill> _skillrepo;
        private readonly ILoginedUser _loginedUser;
        public JobVacanciesController(IJobVacancyRepository repository, IRepository<Skill> skillrepo, ILoginedUser loginedUser) : base(repository)
        {
            _repository = repository;
            _skillrepo = skillrepo;
            _loginedUser = loginedUser;
        }

        [AllowAnonymous]
        public override async Task<ActionResult<JobVacancyDto>> Get(int id)
        {
            var record = _loginedUser.IsAuthorized ? await _repository.GetByIdAsyncCompleteDetails(id) : (await _repository.GetByIdAsync(id)).Adapt<JobVacancyDto>();
            var skills = (await _skillrepo.GetAllAsync(record.JobVacancySkills.Select(x => x.SkillId).ToArray())).Adapt<List<LookupDto>>();
            _populateSkillsData(record.JobVacancySkills, skills);
            return Ok(record);
        }
        [AllowAnonymous]
        public override async Task<ActionResult<PagerDto<JobVacancyDto>>> GetPages(int pageId)
        {
            var pages = (await _repository.GetPage(pageId, forApplicant: !_loginedUser.IsAuthorized)).Adapt<PagerDto<JobVacancyDto>>();
            var skills = (await _skillrepo.GetAllAsync(pages.Data.SelectMany(x => x.JobVacancySkills).Select(x => x.SkillId).ToArray())).Adapt<List<LookupDto>>();
            pages.Data.ForEach(job => _populateSkillsData(job.JobVacancySkills, skills));
            return Ok(pages);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("{JobId}/Apply")]
        public async Task<ActionResult<MessageResponse>> Apply(int jobId ,ApplicantDto applicant)
        {
            var date = DateOnly.FromDateTime(DateTime.Now);
            if (!(await _repository.IsExist(x => x.ValidFrom <= date && x.ValidTo >= date)))
                return BadRequest("JobHasBeenClosed");

            if (_repository.IsApplicantApplaiedBefore(jobId, applicant.Email))
                return BadRequest("YouHaveAppliedBefore");
            await _repository.Apply(jobId, applicant);
            return Ok();
        }

        private void _populateSkillsData(List<JobVacancySkillDto> jobVacancySkills, List<LookupDto> skills)
        {
            jobVacancySkills.ForEach(x =>
            {
                x.LocalName = skills.FirstOrDefault(s => s.Id == x.SkillId).LocalName;
                x.ForeignName = skills.FirstOrDefault(s => s.Id == x.SkillId).ForeignName;
            });
        }
    }
}
