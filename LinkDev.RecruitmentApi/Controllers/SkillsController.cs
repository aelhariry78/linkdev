﻿using LinkDev.CoreApiBuilder.Application.Dtos;
using LinkDev.CoreApiBuilder.Controllers;
using LinkDev.CoreApiBuilder.Domain.Repositories;
using LinkDev.RecruitmentApi.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace LinkDev.RecruitmentApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SkillsController : BaseController<Skill, LookupDto>
    {
        public SkillsController(IRepository<Skill> repository) : base(repository)
        {
        }
    }
}
