﻿using LinkDev.CoreApiBuilder.Controllers;
using LinkDev.CoreApiBuilder.Domain.Repositories;
using LinkDev.RecruitmentApi.Application.Dtos;
using LinkDev.RecruitmentApi.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace LinkDev.RecruitmentApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : BaseController<Category, Category>
    {
        public CategoriesController(IRepository<Category> repository) : base(repository)
        {
        }
    }
}
