﻿using LinkDev.CoreApiBuilder.Infrastructure.Seeds;
using LinkDev.RecruitmentApi.Domain.Repositories;
using LinkDev.RecruitmentApi.Infrastructure.Repositories;

namespace LinkDev.RecruitmentApi
{
    public class Startup : LinkDev.CoreApiBuilder.Startup
    {
        public Startup(IConfiguration configuration) : base(configuration)
        {
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient< IJobVacancyRepository, JobVacancyRepository> ();
            base.ConfigureServices(services);
        }
        public override async void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            base.Configure(app, env, serviceProvider);
            using var scope = serviceProvider.CreateScope();
            var dbInitializers = scope.ServiceProvider.GetServices<IDbInitializer>();
            foreach (var dbInitializer in dbInitializers)
            {
                await dbInitializer.Seed();
            }
        }
    }
}
