﻿using LinkDev.CoreApiBuilder.Domain.Models;
using System.ComponentModel.DataAnnotations;

namespace LinkDev.RecruitmentApi.Domain.Models
{
    public class JobVacancy:Entity
    {
        [MaxLength(150)]
        public string LocalName { get; set; }

        [MaxLength(150)]
        public string ForeignName { get; set; }
        [MaxLength(500)]
        public string LocalDescription { get; set; }
        [MaxLength(500)]
        public string ForeignDesciption { get; set; }
        [MaxLength(500)]
        public string Responsibilities { get; set; } 
        public DateOnly ValidFrom { get; set; }
        public DateOnly ValidTo { get; set; }
        public int MaximumApplication { get; set; }
        public int JobCategoryId { get; set; }
        public virtual Category Category { get; set; }
        public virtual ICollection<JobVacancySkill> JobVacancySkills { get; set; } = new List<JobVacancySkill> ();

    }
}
