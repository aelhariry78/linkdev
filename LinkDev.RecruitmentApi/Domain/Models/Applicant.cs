﻿using LinkDev.CoreApiBuilder.Domain.Models;
using System.ComponentModel.DataAnnotations;

namespace LinkDev.RecruitmentApi.Domain.Models
{
    public class Applicant:Entity
    {
        [MaxLength(150)]
        public string FullName { get; set; }

        [EmailAddress]
        [MaxLength(150)]
        public string Email { get; set; }
        [MaxLength(150)]
        public string PhoneNumber { get; set; }
    }
}
