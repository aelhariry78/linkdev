﻿using LinkDev.CoreApiBuilder.Domain.Models;
using System.ComponentModel.DataAnnotations;

namespace LinkDev.RecruitmentApi.Domain.Models
{
    public class Category:Entity
    {
        [MaxLength(150)]
        public string LocalName { get; set; }
        [MaxLength(150)]
        public string ForeignName { get; set; }
    }
}
