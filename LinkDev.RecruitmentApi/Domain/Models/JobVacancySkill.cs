﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LinkDev.RecruitmentApi.Domain.Models
{
    public class JobVacancySkill
    {
        
        public int JobVacancyId { get; set; }
        
        public int SkillId { get; set; }
    }
}
