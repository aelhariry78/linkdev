﻿namespace LinkDev.RecruitmentApi.Domain.Models
{
    public class JobVacancyApplication
    {
        public int ApplicationId { get; set; }
        public int JobVacanyId { get; set; }
        public DateTime AppliedAt { get; set; }
    }
}
