﻿using LinkDev.CoreApiBuilder.Application.Dtos;
using LinkDev.CoreApiBuilder.Domain.Repositories;
using LinkDev.RecruitmentApi.Application.Dtos;
using LinkDev.RecruitmentApi.Domain.Models;

namespace LinkDev.RecruitmentApi.Domain.Repositories
{
    public interface IJobVacancyRepository: IRepository<JobVacancy>
    {
        bool IsApplicantApplaiedBefore(int jobId, string email);
        Task Apply(int jobId, ApplicantDto applicant);
        Task<JobVacancyDto> GetByIdAsyncCompleteDetails(int id);
        Task<PagerDto<JobVacancy>> GetPage(int page, int length = 3, bool forApplicant = false);
    }
}
