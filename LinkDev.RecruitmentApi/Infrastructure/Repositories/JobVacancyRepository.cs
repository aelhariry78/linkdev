﻿using LinkDev.CoreApiBuilder.Application.Dtos;
using LinkDev.CoreApiBuilder.Domain.Repositories;
using LinkDev.CoreApiBuilder.Infrastructure;
using LinkDev.CoreApiBuilder.Infrastructure.Repositories;
using LinkDev.RecruitmentApi.Application.Dtos;
using LinkDev.RecruitmentApi.Domain.Models;
using LinkDev.RecruitmentApi.Domain.Repositories;
using Mapster;
using Microsoft.EntityFrameworkCore;

namespace LinkDev.RecruitmentApi.Infrastructure.Repositories
{
    public class JobVacancyRepository : Repository<JobVacancy>, IJobVacancyRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly IRepository<Applicant> _applicantRepository;
        public JobVacancyRepository(ApplicationDbContext context, IRepository<Applicant> applicantRepository) : base(context)
        {
            _context = context;
            _applicantRepository = applicantRepository;

        }

        public  async Task<JobVacancyDto> GetByIdAsyncCompleteDetails(int id)
        {
            var vacancy = (await base.GetByIdAsync(id)).Adapt<JobVacancyDto>();

            vacancy.Applicants = (await (from application in _context.Set<JobVacancyApplication>()
                                         join applicant in _context.Set<Applicant>() on application.ApplicationId equals applicant.Id
                                         where application.JobVacanyId == id
                                         orderby application.AppliedAt
                                         select new { applicant.FullName, applicant.PhoneNumber, applicant.Email, applicant.Id, application.AppliedAt } 
                                         )
                                .ToListAsync()).Adapt<List<ApplicantDto>>();
            
            return vacancy;
        }
        public bool IsApplicantApplaiedBefore(int jobId, string email)
        {
           return  (from application in  _context.Set<JobVacancyApplication>()
            join applicant in _context.Set<Applicant>() on application.ApplicationId equals applicant.Id
            where applicant.Email == email && application.JobVacanyId == jobId
            select applicant).Count() > 0;
        }
        public async Task Apply(int jobId, ApplicantDto applicant)
        {
            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                var record = await _applicantRepository.CreateAsync(applicant.Adapt<Applicant>());
                await _context.Set<JobVacancyApplication>().AddAsync(new JobVacancyApplication() { AppliedAt = DateTime.Now, ApplicationId = record.Id, JobVacanyId = jobId });
                _context.SaveChanges();
                dbContextTransaction.Commit();

            }
        }

        public async Task<PagerDto<JobVacancy>> GetPage(int page, int length = 3, bool forApplicant= false)
        {
            var skip = page * length;
            var date = DateOnly.FromDateTime(DateTime.Now);
            var query = forApplicant ? AsQueryable().Where(x => x.ValidFrom <= date && x.ValidTo >= date) : AsQueryable(); 
            return new()
            {
                Data = await query.Skip(skip).Take(length).ToListAsync(),
                Total = AsQueryable().Count()
            };
        }
    }
}
