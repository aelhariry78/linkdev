﻿using LinkDev.CoreApiBuilder.Domain.Repositories;
using LinkDev.CoreApiBuilder.Infrastructure.Seeds;
using LinkDev.RecruitmentApi.Domain.Models;

namespace LinkDev.RecruitmentApi.Infrastructure.Presistance.Seeds
{
    public class CategoryInitializer : IDbInitializer
    {
        private readonly IRepository<Category> _categoryRepo;

        public CategoryInitializer(IRepository<Category> categoryRepo)
        {
            _categoryRepo = categoryRepo;
        }

        public async Task Seed()
        {
            if (!_categoryRepo.IsExist(x => x.ForeignName == "HR").Result)
                await  _categoryRepo.CreateAsync(new Category() { LocalName = "موارد بشرية", ForeignName = "HR" });


            if (!_categoryRepo.IsExist(x => x.ForeignName == "Accountant").Result)
               await  _categoryRepo.CreateAsync(new Category() { LocalName = "حسابات", ForeignName = "Accountant" });


            if (!_categoryRepo.IsExist(x => x.ForeignName == "DBA").Result)
               await _categoryRepo.CreateAsync(new Category() { LocalName = "مدير قواعد بيانات", ForeignName = "DBA" });


            if (!_categoryRepo.IsExist(x => x.ForeignName == "Designer").Result)
              await  _categoryRepo.CreateAsync(new Category() { LocalName = "مصمم", ForeignName = "Designer" });
        }
    }
}
