﻿using LinkDev.CoreApiBuilder.Infrastructure.Seeds;
using LinkDev.CoreApiBuilder.Infrastructure;
using LinkDev.CoreApiBuilder.Domain.Repositories;
using LinkDev.RecruitmentApi.Domain.Models;

namespace LinkDev.RecruitmentApi.Infrastructure.Presistance.Seeds
{
    public class SkillsInitializer : IDbInitializer
    {
        private readonly IRepository<Skill> _skillRepo;
        public SkillsInitializer(IRepository<Skill> skillRepo)
        {
            _skillRepo = skillRepo;
        }
        
        public async Task Seed()
        {
            if (!(await _skillRepo.IsExist(x => x.LocalName == "C#")))
               await _skillRepo.CreateAsync(new Skill() { LocalName = "C#", ForeignName = "C#" });


            if (! (await _skillRepo.IsExist(x => x.LocalName == "Sql")))
               await _skillRepo.CreateAsync(new Skill() { LocalName = "sql", ForeignName = "sql" });


            if (!_skillRepo.IsExist(x => x.LocalName == "ef").Result)
               await _skillRepo.CreateAsync(new Skill() { LocalName = "ef", ForeignName = "ef" });


            if (!_skillRepo.IsExist(x => x.LocalName == "writing").Result)
               await _skillRepo.CreateAsync(new Skill() { LocalName = "writing", ForeignName = "writing" });
        }
    }
}
