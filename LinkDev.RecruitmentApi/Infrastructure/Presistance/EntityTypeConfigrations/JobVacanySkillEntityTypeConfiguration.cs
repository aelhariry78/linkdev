﻿using LinkDev.RecruitmentApi.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LinkDev.RecruitmentApi.Infrastructure.Presistance.EntityTypeConfigrations
{
    public class JobVacanySkillEntityTypeConfiguration : IEntityTypeConfiguration<JobVacancySkill>
    {
        public void Configure(EntityTypeBuilder<JobVacancySkill> builder)
        {
            builder.ToTable(nameof(JobVacancy.JobVacancySkills));
            builder.HasKey(nameof(JobVacancySkill.SkillId), nameof(JobVacancySkill.JobVacancyId));

            builder.HasOne<JobVacancy>().WithMany(x => x.JobVacancySkills).
                HasForeignKey(x => x.JobVacancyId).
                HasPrincipalKey(x => x.Id).OnDelete(DeleteBehavior.NoAction);

            builder.HasOne<Skill>().WithMany().
                HasForeignKey(x => x.SkillId).
                HasPrincipalKey(x => x.Id)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}
