﻿using LinkDev.RecruitmentApi.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LinkDev.RecruitmentApi.Infrastructure.Presistance.EntityTypeConfigrations
{
    public class JobVacancyApplicationEntityTypeConfiguration : IEntityTypeConfiguration<JobVacancyApplication>
    {
        public void Configure(EntityTypeBuilder<JobVacancyApplication> builder)
        {
            builder.HasKey(x => new { x.JobVacanyId, x.ApplicationId });
            builder.Property(x => x.AppliedAt).HasDefaultValueSql("getDate()");
            builder.ToTable("JobVacancyApplications");
            builder.HasOne<JobVacancy>().WithMany().HasForeignKey(x => x.JobVacanyId).OnDelete(DeleteBehavior.NoAction);
            builder.HasOne<Applicant>().WithMany().HasForeignKey(x => x.ApplicationId).OnDelete(DeleteBehavior.NoAction);
        }
    }
}
