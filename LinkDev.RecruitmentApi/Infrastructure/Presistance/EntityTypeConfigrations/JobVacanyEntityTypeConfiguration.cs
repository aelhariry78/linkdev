﻿using LinkDev.RecruitmentApi.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations.Schema;

namespace LinkDev.RecruitmentApi.Infrastructure.Presistance.EntityTypeConfigrations
{
    public class JobVacanyEntityTypeConfiguration : IEntityTypeConfiguration<JobVacancy>
    {
        public void Configure(EntityTypeBuilder<JobVacancy> builder)
        {
            //builder.HasMany(x => x.JobVacancySkills).
            //        WithOne().
            //        HasForeignKey(x => x.JobVacancyId);

            builder.HasOne(x => x.Category).WithMany().HasForeignKey(x => x.JobCategoryId);
        }
    }
}
