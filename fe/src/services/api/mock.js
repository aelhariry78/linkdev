import { TOKEN_KEY } from '~/configs/keys';

export default {
  login: () => new Promise((resolve) => {
    setInterval(() => {
      window.localStorage.setItem(TOKEN_KEY, 'TOKEN');
      resolve('TOKEN');
    }, 600);
  }),
};
