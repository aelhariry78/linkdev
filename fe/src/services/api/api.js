import { REPORTING_URL } from '~/configs/keys';
import { $axios } from '~/plugins/axios';

export default {
  /**
   * login api
   * @param {string} username
   * @param {string} password
   * @returns {Promise} Promise object represent the returned value
   */
  login: (username, password) => $axios.post('/api/Auth', { username: username, password: password }),
  gateway_old: {
    get: (entityName, id) => $axios.get(`/api/Gateway/${entityName}${id ? `/${id}` : ''}`),
    create: (entityName, data) => $axios.post(`/api/Gateway/${entityName}`, data),
    update: (entityName, data) => $axios.put(`/api/Gateway/${entityName}`, data),
    delete: (entityName, id) => $axios.delete(`/api/Gateway/${entityName}/${id}`),
  },
  /**
   * backend services
   */
  gateway: {
    /**
     * get Api
     * @param {string} resourceUrl - resource url
     * @param {Number} id - id for the entity you need
     * @returns {Promise} Promise object represent the returned value
     */
    get: (resourceUrl, id) => $axios.get(`/api/${resourceUrl}${id ? `/${id}` : ''}`),
    /**
     * post APi
     * @param {string} resourceUrl - resource url
     * @param {object} data created entity
     * @returns {Promise} Promise object represent the returned value
     */
    create: (resourceUrl, data) => $axios.post(`/api/${resourceUrl}`, data),
    /**
     * put APi
     * @param {string} resourceUrl - resource url
     * @param {object} data updated entity
     * @returns {Promise} Promise object represent the returned value
     */
    update: (resourceUrl, data) => $axios.put(`/api/${resourceUrl}`, data),
    /**
     * delete APi
     * @param {string} resourceUrl - resource url
     * @param {number} id id of the deleted dentity
     * @returns {Promise} Promise object represent the returned value
     */
    delete: (resourceUrl, id) => $axios.delete(`/api/${resourceUrl}/${id}`),
  },
  get: (resourceUrl, id) => $axios.get(`/api/${resourceUrl}${id ? `/${id}` : ''}`),
  viewReport: (reportName = '', model = {}) => {
    const queryString = btoa(unescape(encodeURIComponent(JSON.stringify(model))));
    window.open(`${REPORTING_URL}?reportName=${reportName}&q=${queryString}`);
  },
};
