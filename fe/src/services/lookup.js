import { menu } from '~/configs/menu';

export const Menu = (() => {
    const m = [];
    const getItems = (item) => {
        item.forEach((element) => {
            if (element.list) {
                getItems(element.list);
            } else {
                m.push(element);
            }
        });
    };
    getItems(menu);
    return m;
})();