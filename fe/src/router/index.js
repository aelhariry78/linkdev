import Vue from 'vue';
import VueRouter from 'vue-router';

import { TOKEN_KEY } from '~/configs/keys';

Vue.use(VueRouter);

const routes = [{
        path: '/',
        component: () =>
            import ('~/layouts/main.vue'),
        children: [{
                path: '',
                redirect: 'home',
            },
            {
                path: 'login',
                name: 'login',
                component: () =>
                    import ('~/pages/login.vue'),
            },
            {
                path: 'home',
                name: 'home',
                component: () =>
                    import ('~/pages/home/index.vue'),
            },
            {
                path: 'apply/:id',
                name: 'apply',
                component: () =>
                    import ('~/pages/jobApply/index.vue'),
                props: true,
            },
            {
                path: 'skills',
                component: () =>
                    import ('~/components/navigationIndex.vue'),
                children: [{
                        path: '',
                        redirect: 'list',
                    },
                    {
                        path: 'list',
                        component: () =>
                            import ('~/pages/skills/List.vue'),
                    },
                    {
                        path: 'create',
                        component: () =>
                            import ('~/pages/skills/Edit.vue'),
                    },
                    {
                        path: ':id',
                        component: () =>
                            import ('~/pages/skills/Edit.vue'),
                        props: true,
                    },
                ],
            },
            {
                path: 'jobVacancies',
                component: () =>
                    import ('~/components/navigationIndex.vue'),
                children: [{
                        path: '',
                        redirect: 'list',
                    },
                    {
                        path: 'list',
                        component: () =>
                            import ('~/pages/jobVacancies/List.vue'),
                    },
                    {
                        path: 'create',
                        component: () =>
                            import ('~/pages/jobVacancies/Edit.vue'),
                    },
                    {
                        path: ':id',
                        component: () =>
                            import ('~/pages/jobVacancies/Edit.vue'),
                        props: true,
                    },
                ],
            },
            {
                path: 'categories',
                component: () =>
                    import ('~/components/navigationIndex.vue'),
                children: [{
                        path: '',
                        redirect: 'list',
                    },
                    {
                        path: 'list',
                        component: () =>
                            import ('~/pages/categories/List.vue'),
                    },
                    {
                        path: 'create',
                        component: () =>
                            import ('~/pages/categories/Edit.vue'),
                    },
                    {
                        path: ':id',
                        component: () =>
                            import ('~/pages/categories/Edit.vue'),
                        props: true,
                    },
                ],
            },
        ],
        beforeEnter: (to, from, next) => {
            if (to.query.token) {
                window.localStorage.setItem(TOKEN_KEY, to.query.token);
                // TODO: Get Profile Before next()
                next(to.path);
            } else if (window.localStorage.getItem(TOKEN_KEY) || to.path == '/home' || to.name == 'apply') {
                next();
            } else if (to.path !== '/login') {
                next('home');
            }
            next();
        },
    },
    {
        path: '/error',
        component: () =>
            import ('~/view/Error.vue'),
        children: [{
            path: 'un-authorize',
            name: 'unAuthorize',
            component: () =>
                import ('~/view/Error-1.vue'),
        }, ],
    },
];

const router = new VueRouter({
    // mode: 'history',
    routes,
});

export default router;