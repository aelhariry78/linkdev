export const TOKEN_KEY = '@MADRASTY_TOKEN';
export const PROFILE_KEY = '@MADRASTY_PROFILE';
export const BRANCH_ID_KEY = '@MADRASTY_BRANCH_ID';
export const LOGIN_URL = 'https://localhost:7228';
export const BACKEND_URL = 'https://localhost:7228'; // 'https://localhost:5001'; // 'http://162.220.165.9:5050';