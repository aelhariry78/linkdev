import Vue from 'vue';

import {
  ValidationProvider,
  ValidationObserver,
  extend,
  localize,
} from 'vee-validate';

import {
  required,
  email,
  digits,
  min,
  max,
  max_value,
  min_value,
} from 'vee-validate/dist/rules';

import ar from 'vee-validate/dist/locale/ar.json';

// Global Components
Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);
// Validators
extend('email', email);
extend('required', required);
extend('digits', digits);
extend('min', min);
extend('max', max);
extend('min_value', min_value);
extend('max_value', max_value);
extend('password', {
  params: ['target'],

  validate(value, params) {
    return value === params.target;
  },

  message: 'كلمات المرور غير متطابقة',
});
extend('url', {
  validate(value) {
    // eslint-disable-next-line no-useless-escape
    const urlRegx = /[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/ig;
    return urlRegx.test(value);
  },

  message: 'ادخل رابط صحيح',
});
extend('phoneNumber', {
  validate(value) {
    return value && value[0] === '5' && value.length === 9;
  },

  message: 'رقم الجوال يجب ان يبدأ ب 5 وان يكون مكون من 9 ارقام',
});
extend('isTrue', {
  validate(value) {
    return !!value;
  },

  message: '',
});

localize('ar', ar);
