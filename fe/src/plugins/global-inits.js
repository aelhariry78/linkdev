import Vue from 'vue';
import VueApexCharts from 'vue-apexcharts';

import GForm from '~/components/form/index.vue';
import GFormField from '~/components/form/layout/field.vue';
import GInputs from '~/components/form/inputs/index';
import Gbool from '~/components/bool';

import TreeView from '~/components/tree/treeView.vue';

Vue.use(VueApexCharts);

Vue.component('Apexchart', VueApexCharts);
Vue.component('TreeView', TreeView);

// Set Global Components
Vue.component('GForm', GForm);
Vue.component('GFormField', GFormField);
Vue.component('GBool', Gbool);

// Iterate Inputs
Object.keys(GInputs).forEach((key) => {
  Vue.component(key, GInputs[key]);
});
