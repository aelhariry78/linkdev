import Vue from 'vue';
import axios from 'axios';
import {
    TOKEN_KEY,
    // BACKEND_URL,
    PROFILE_KEY,
    BRANCH_ID_KEY,
} from '~/configs/keys';

// Main instance
const $axios = axios.create({
    // baseURL: process.env.API_BASE_URL || BACKEND_URL,
});
axios.defaults.withCredentials = false;

$axios.interceptors.request.use((request) => {
    const token = window.localStorage.getItem(TOKEN_KEY);
    if (token) {
        request.headers.Authorization = `Bearer ${token}`;
    }

    request.headers.Branch = window.localStorage.getItem(BRANCH_ID_KEY) || 1;

    return request;
});

$axios.interceptors.response.use(undefined, (error) => {
    const token = window.localStorage.getItem(TOKEN_KEY);

    if (error.response.status === 401 && token) {
        window.localStorage.removeItem(TOKEN_KEY);
        window.localStorage.removeItem(PROFILE_KEY);
        window.location.reload();
        // window.location.replace(ADMIN_URL);
    }

    return Promise.reject(error);
});

// for use inside Vue files through this.$axios
Vue.prototype.$axios = $axios;

export { $axios };