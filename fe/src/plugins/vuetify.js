import '@mdi/font/css/materialdesignicons.css';
import Vue from 'vue';
import Vuetify from 'vuetify/lib';

import ar from '../locales/ar';
import en from '../locales/en';

Vue.use(Vuetify);

export default new Vuetify({
    icons: {
        iconfont: 'mdiSvg', //  'mdi',
    },
    lang: {
        locales: { ar, en },
        current: 'en',
    },
    theme: {
        themes: {
            light: {
                primary: '#3f51b5',
                secondary: '#b0bec5',
                accent: '#8c9eff',
                error: '#b71c1c',
            },
            // light: {
            //   primary: '#5867dd',
            //   secondary: '#e8ecfa',
            //   accent: '#5d78ff',
            //   error: '#b71c1c', // '#fd397a',
            //   info: '#5578eb',
            //   success: '#0abb87',
            //   warning: '#ffb822',
            // },
        },
    },
});