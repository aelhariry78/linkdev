import GTextInput from './text.vue';
import GLookupInput from './lookup.vue';
import GDateInput from './date.vue';
import GTimeInput from './time.vue';

export default {
  GTextInput,
  GLookupInput,
  GDateInput,
  GTimeInput,
};
