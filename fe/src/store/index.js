import Vue from 'vue';
import Vuex from 'vuex';

import actions from './actions';
import state from './state';
import mutations from './mutations';
import getters from './getters';

import AppModule from './modules/app';

Vue.use(Vuex);

const Store = new Vuex.Store({
    modules: {
        AppModule,
    },

    actions,
    mutations,
    getters,
    state,

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: !!process.env.DEV,
});

// Dispatch Root Action
// Store.dispatch('AuthenticationModule/check');

export default Store;