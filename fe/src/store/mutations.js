const mutation = {
    setLoading(state, loading) {
        state.loading = !!loading;
    },
    setNotify(state, notify) {
        state.notify = notify;
    },
    setLang(state, lang) {
        state.lang = lang;
    },
    setUserData(state, userData) {
        state.userData = userData;
    },
};

export default mutation;