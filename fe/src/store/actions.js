const actions = {
  showLoading({ commit }) {
    commit('setLoading', true);
  },
  hideLoading({ commit }) {
    commit('setLoading', false);
  },
  notify({ commit, dispatch }, data) {
    const notifyData = {
      key: Math.random(),
      text: data.text,
      visible: true,
      duration: 1000,
      color: data.color || 'success',
    };

    commit('setNotify', notifyData);

    setTimeout(() => dispatch('hideNotify'), 5000000);
  },
  hideNotify({ commit }) {
    const defaultNotifyData = {
      text: '',
      visible: false,
      color: 'success',
    };

    commit('setNotify', defaultNotifyData);
  },
  setLang({ commit }, lang) {
    localStorage.setItem('language', lang);
    commit('setLang', lang);
  },
};

export default actions;
