const actions = {
  updateAccounts({ state }, account) {
    const item = state.accounts.find(((a) => a.id === account.id));
    if (item && item.id > 0) {
      Object.assign(item, account);
    } else {
      state.accounts.push(account);
    }
  },
  removeAccount({ state }, account) {
    state.accounts = state.accounts.filter((item) => item.id !== account.id);
  },
  updateCostCenters({ state }, costCenter) {
    const item = state.costCenters.find(((a) => a.id === costCenter.id));
    if (item && item.id > 0) {
      Object.assign(item, costCenter);
    } else {
      state.costCenters.push(costCenter);
    }
  },
  removeCostCenter({ state }, costCenter) {
    state.costCenters = state.costCenters.filter((item) => item.id !== costCenter.id);
  },
  updateAssets({ state }, asset) {
    const item = state.assets.find(((a) => a.id === asset.id));
    if (item && item.id > 0) {
      Object.assign(item, asset);
    } else {
      state.assets.push(asset);
    }
  },
  removeAsset({ state }, asset) {
    state.assets = state.assets.filter((item) => item.id !== asset.id);
  },
};

export default actions;
