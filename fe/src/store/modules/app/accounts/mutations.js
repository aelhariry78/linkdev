const mutation = {
  setAccounts(state, accounts) {
    state.accounts = accounts;
  },
  setCostCenters(state, costCenters) {
    state.costCenters = costCenters;
  },
  setAssets(state, assets) {
    state.assets = assets;
  },
};

export default mutation;
