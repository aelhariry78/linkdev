const getters = {
  accountsGetter: (state) => JSON.parse(JSON.stringify(state.accounts)),
  mainAccounts: (state) => state.accounts.filter((item) => item.type === 'main'),
  subAccounts: (state) => state.accounts.filter((item) => item.type === 'sub'),
  isAccountsLoaded: (state) => state.accounts.length > 0,
  accountGetter: (state) => ({ code, id }) => {
    const account = code ? state.accounts.find((item) => item.code == code)
      : state.accounts.find((item) => item.id == id);
    return account || { code: 0, id: 0 };
  },
  costCentersGetter: (state) => JSON.parse(JSON.stringify(state.costCenters)),
  mainCostCenter: (state) => state.costCenters.filter((item) => item.type === 'main'),
  subCostCenter: (state) => state.costCenters.filter((item) => item.type === 'sub'),
  costCenterGetter: (state) => ({ code, id }) => {
    const costCenter = code ? state.costCenters.find((item) => item.code == code)
      : state.costCenters.find((item) => item.id == id);
    return costCenter || { code: 0, id: 0 };
  },
  assetsGetter: (state) => JSON.parse(JSON.stringify(state.assets)),
  mainAssets: (state) => state.assets.filter((item) => item.type === 'main'),
  subAssets: (state) => state.assets.filter((item) => item.type === 'sub'),
  assetGetter: (state) => ({ code, id }) => {
    const account = code ? state.assets.find((item) => item.code == code)
      : state.assets.find((item) => item.id == id);
    return account || { code: 0, id: 0 };
  },
};

export default getters;
