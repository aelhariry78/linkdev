import api from '~/services/api';
import Router from '~/router';
import Vuetify from '~/plugins/vuetify';

const actions = {
    /**
     * get Api
     * @param {Object} vuex objects to handle mutations and
     * @param {Object} data {id , resourceUrl}
     * @returns
     */
    get({ dispatch }, { id, gateway }) {
        dispatch('showLoading', null, { root: true });
        return new Promise((resolve, reject) => {
            api.get(gateway, id).then(({ data }) => {
                resolve(data);
            }).catch(() => {
                reject();
                dispatch('notify', { text: Vuetify.framework.lang.t('$vuetify.serverError'), color: 'error' }, { root: true });
            }).finally(() => {
                dispatch('hideLoading', null, { root: true });
            });
        });
    },
    getResource({ dispatch }, { resource, id }) {
        dispatch('showLoading', null, { root: true });
        return new Promise((resolve, reject) => {
            api.get(resource, id).then(({ data }) => {
                resolve(data);
            }).catch(() => {
                reject();
                // Router.push('/home');
                dispatch('notify', {
                    text: Vuetify.framework.lang.t('$vuetify.serverError'),
                    color: 'error',
                }, {
                    root: true,
                });
            }).finally(() => {
                dispatch('hideLoading', null, { root: true });
            });
        });
    },
    create({ dispatch }, { gateway, data, stay }) {
        // Show loading
        dispatch('showLoading', null, { root: true });
        return new Promise((resolve, reject) => {
            api.gateway.create(gateway, data).then((res) => {
                dispatch('notify', {
                    text: Vuetify.framework.lang.t('$vuetify.serverSuccessPost'),
                }, { root: true });
                if (stay !== true) Router.back();
                resolve(res.data);
            }).catch(({ response }) => {
                dispatch('notify', {
                    text: response.data ? Vuetify.framework.lang.t(`$vuetify.${response.data}`) : Vuetify.framework.lang.t('$vuetify.serverError'),
                    color: 'error',
                }, { root: true });
                reject();
            }).finally(() => {
                dispatch('hideLoading', null, { root: true });
            });
        });
    },
    update({ dispatch }, { gateway, data, id }) {
        // Show loading
        dispatch('showLoading', null, { root: true });
        return new Promise((resolve, reject) => {
            api.gateway.update(`${gateway}/${id}`, data).then(() => {
                if (JSON.stringify(Router.history.current.params) !== JSON.stringify({})) {
                    Router.back();
                }
                dispatch('notify', {
                    text: Vuetify.framework.lang.t('$vuetify.serverSuccessPut'),
                }, { root: true });
                resolve(data);
            }).catch(() => {
                dispatch('notify', {
                    text: Vuetify.framework.lang.t('$vuetify.serverError'),
                    color: 'error',
                }, { root: true });
                reject();
            }).finally(() => {
                dispatch('hideLoading', null, { root: true });
            });
        });
    },
    delete({ dispatch }, { gateway, id }) {
        // Show loading
        dispatch('showLoading', null, { root: true });
        return new Promise((resolve, reject) => {
            api.gateway.delete(gateway, id).then((data) => {
                if (JSON.stringify(Router.history.current.params) !== JSON.stringify({})) {
                    Router.back();
                }
                dispatch('notify', {
                    text: Vuetify.framework.lang.t('$vuetify.serverSuccessDelete'),
                }, { root: true });
                resolve(data);
            }).catch(() => {
                dispatch('notify', {
                    text: Vuetify.framework.lang.t('$vuetify.serverError'),
                    color: 'error',
                }, { root: true });
                reject();
            }).finally(() => {
                dispatch('hideLoading', null, { root: true });
            });
        });
    },
};

export default actions;