const mutation = {
  set(state, { entityName, obj }) {
    state[entityName] = obj;
  },
};

export default mutation;
