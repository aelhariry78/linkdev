import GatewayModule from './gateway';

export default {
  namespaced: true,

  // Child modules
  modules: {
    GatewayModule,
  },
};
