import GeneralCodesModule from './generalCodes';
import AuthModule from './auth';

export default {
    namespaced: true,

    // Child modules
    modules: {
        AuthModule,
        GeneralCodesModule,
    },
};