import { TOKEN_KEY, PROFILE_KEY } from '~/configs/keys';

const mutation = {
  setToken(state, token) {
    localStorage.setItem(TOKEN_KEY, token);
    state.token = token;
  },
  setProfile(state, profile) {
    localStorage.setItem(PROFILE_KEY, JSON.stringify(profile));
    state.profile = profile;
  },
};

export default mutation;
