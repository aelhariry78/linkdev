import api from '~/services/api/api';
import Router from '~/router';
import Vuetify from '~/plugins/vuetify';
import { TOKEN_KEY, PROFILE_KEY } from '~/configs/keys';

const actions = {
  login({ commit, dispatch }, { userName, password }) {
    dispatch('showLoading', null, { root: true });
    api.login(userName, password).then(({ data }) => {
      commit('setToken', data.token);
      commit('setProfile', data);
      Router.push('/home');
    }).catch(() => {
      dispatch('notify',
        {
          text: Vuetify.framework.lang.t('$vuetify.serverError'),
          color: 'error',
        },
        {
          root: true,
        });
    }).finally(() => {
      dispatch('hideLoading', null, { root: true });
    });
  },
  logout({ commit }) {
    window.localStorage.removeItem(TOKEN_KEY);
    window.localStorage.removeItem(PROFILE_KEY);
    commit('setToken', '');
    commit('setProfile', {});
    Router.push('/login');
  },
};

export default actions;
