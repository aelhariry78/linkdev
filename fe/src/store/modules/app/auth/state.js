import { TOKEN_KEY, PROFILE_KEY } from '~/configs/keys';

const state = {
  token: localStorage.getItem(TOKEN_KEY),
  profile: JSON.parse(localStorage.getItem(PROFILE_KEY)) || { isAdmin: false },
};

export default state;
