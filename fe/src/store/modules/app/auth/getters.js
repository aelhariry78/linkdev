const getters = {
  token: ({ token }) => token,
  profile: ({ profile }) => profile,
};

export default getters;
