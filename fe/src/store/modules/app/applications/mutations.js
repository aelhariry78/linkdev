const mutation = {
  setApps(state, apps) {
    state.apps = apps;
  },
};

export default mutation;
