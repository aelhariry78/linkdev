import api from '~/services/api';
import Vuetify from '~/plugins/vuetify';

import { ADMIN_URL } from '~/configs/keys';

const actions = {
  getApps({ commit, dispatch }) {
    // Show loading
    dispatch('showLoading', null, { root: true });

    api
      .getApps()
      .then(({ data }) => {
        commit('setApps', data);
      })
      .catch(() => {
        dispatch('notify',
          {
            text: Vuetify.framework.lang.t('$vuetify.serverError'),
            color: 'error',
          },
          {
            root: true,
          });

        window.location.replace(ADMIN_URL);
      })
      .finally(() => {
        dispatch('hideLoading', null, { root: true });
      });
  },
};

export default actions;
