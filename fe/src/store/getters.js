const getters = {
    notify: (state) => state.notify,
    lang: (state) => state.lang,
    profile: (state) => JSON.parse(state.profile),
};

export default getters;