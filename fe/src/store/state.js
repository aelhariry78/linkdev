import { PROFILE_KEY } from '~/configs/keys';

const state = {
    loading: false,
    lang: localStorage.getItem('language') || 'en',
    profile: localStorage.getItem(PROFILE_KEY) || '{}',
    notify: {
        text: '',
        visible: false,
        color: 'success',
    },
};

export default state;